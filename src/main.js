import Vue from "vue";
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue";
import { Drag, Drop } from "vue-drag-drop";

Vue.component("drag", Drag);
Vue.component("drop", Drop);
Vue.use(BootstrapVue);
/* eslint-disable no-unused-vars */
import store from "./store/index";
/* eslint-enable no-unused-vars */
require("@/assets/css/styles.min.css");
import "popper.js";
// import "bootstrap";
Vue.config.productionTip = false;

import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
