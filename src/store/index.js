import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import customEl from "./customElement";
/* eslint-disable no-unused-vars */
const debug = process.env.NODE_ENV !== "production";
/* eslint-enable no-unused-vars */
export default new Vuex.Store({
  modules: {
    customEl
  },
  strict: false
});
