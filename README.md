# Page-builder
```
- A page builder app with drag and drop elements
- Add one or two columns
- Add headline or image
- Open settings to add headline or image.
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### How to use drag and drop
```
1. add a new row -> add one or more column 
2. open elements tab from navbar and hold the element, drag and drop it on the added row.
```
